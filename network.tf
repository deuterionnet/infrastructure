module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 6.0"

  project_id   = google_project.main.project_id
  network_name = "default"
  description  = "Default network for the project"
  routing_mode = "REGIONAL"

  auto_create_subnetworks = true

  subnets = [
    {
      subnet_name   = "default"
      subnet_ip     = "10.128.0.0/20"
      subnet_region = "us-central1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.132.0.0/20"
      subnet_region = "europe-west1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.138.0.0/20"
      subnet_region = "us-west1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.140.0.0/20"
      subnet_region = "asia-east1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.142.0.0/20"
      subnet_region = "us-east1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.146.0.0/20"
      subnet_region = "asia-northeast1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.148.0.0/20"
      subnet_region = "asia-southeast1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.150.0.0/20"
      subnet_region = "us-east4"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.152.0.0/20"
      subnet_region = "australia-southeast1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.154.0.0/20"
      subnet_region = "europe-west2"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.156.0.0/20"
      subnet_region = "europe-west3"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.158.0.0/20"
      subnet_region = "southamerica-east1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.160.0.0/20"
      subnet_region = "asia-south1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.162.0.0/20"
      subnet_region = "northamerica-northeast1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.164.0.0/20"
      subnet_region = "europe-west4"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.166.0.0/20"
      subnet_region = "europe-north1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.168.0.0/20"
      subnet_region = "us-west2"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.170.0.0/20"
      subnet_region = "asia-east2"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.172.0.0/20"
      subnet_region = "europe-west6"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.174.0.0/20"
      subnet_region = "asia-northeast2"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.178.0.0/20"
      subnet_region = "asia-northeast3"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.180.0.0/20"
      subnet_region = "us-west3"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.182.0.0/20"
      subnet_region = "us-west4"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.184.0.0/20"
      subnet_region = "asia-southeast2"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.186.0.0/20"
      subnet_region = "europe-central2"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.188.0.0/20"
      subnet_region = "northamerica-northeast2"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.190.0.0/20"
      subnet_region = "asia-south2"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.192.0.0/20"
      subnet_region = "australia-southeast2"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.194.0.0/20"
      subnet_region = "southamerica-west1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.196.0.0/20"
      subnet_region = "us-east7"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.198.0.0/20"
      subnet_region = "europe-west8"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.200.0.0/20"
      subnet_region = "europe-west9"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.202.0.0/20"
      subnet_region = "us-east5"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.204.0.0/20"
      subnet_region = "europe-southwest1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.206.0.0/20"
      subnet_region = "us-south1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.208.0.0/20"
      subnet_region = "me-west1"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.210.0.0/20"
      subnet_region = "europe-west12"
    },
    {
      subnet_name   = "default"
      subnet_ip     = "10.212.0.0/20"
      subnet_region = "me-central1"
    },
  ]

  firewall_rules = [
    {
      name        = "default-allow-icmp"
      direction   = "INGRESS"
      priority    = 65534
      description = "Allow ICMP from anywhere"
      ranges      = ["0.0.0.0/0"]
      allow = [{
        ports    = []
        protocol = "icmp"
      }]
    },
    {
      name        = "default-allow-internal"
      direction   = "INGRESS"
      priority    = 65534
      description = "Allow internal traffic on the default network"
      ranges      = ["10.128.0.0/9"]
      allow = [
        {
          ports    = ["0-65535"]
          protocol = "tcp"
        },
        {
          ports    = ["0-65535"]
          protocol = "udp"
        },
        {
          ports    = []
          protocol = "icmp"
        },
      ]
    },
    {
      name        = "default-allow-rdp"
      direction   = "INGRESS"
      priority    = 65534
      description = "Allow RDP from anywhere"
      ranges      = ["0.0.0.0/0"]
      allow = [{
        ports    = ["3389"]
        protocol = "tcp"
      }]
    },
    {
      name        = "default-allow-ssh"
      direction   = "INGRESS"
      priority    = 65534
      description = "Allow SSH from anywhere"
      ranges      = ["0.0.0.0/0"]
      allow = [{
        ports    = ["22"]
        protocol = "tcp"
      }]
    },
  ]
}
