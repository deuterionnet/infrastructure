# Resources identity variables
product = "dn"
env     = "dev"
stack   = "infra"
app     = "general"

# Resources provider variables
region     = "us-west1"
project_id = "deuterion-net"

# Resources variables
billing_account = "016F95-2808BC-46EB7F"
project_services = [
  "artifactregistry.googleapis.com",
  "bigquery.googleapis.com",
  "bigquerystorage.googleapis.com",
  "cloudapis.googleapis.com",
  "cloudasset.googleapis.com",
  "cloudresourcemanager.googleapis.com",
  "cloudtrace.googleapis.com",
  "compute.googleapis.com",
  "container.googleapis.com",
  "containerregistry.googleapis.com",
  "datastore.googleapis.com",
  "dns.googleapis.com",
  "iam.googleapis.com",
  "iamcredentials.googleapis.com",
  "logging.googleapis.com",
  "monitoring.googleapis.com",
  "oslogin.googleapis.com",
  "pubsub.googleapis.com",
  "servicemanagement.googleapis.com",
  "serviceusage.googleapis.com",
  "sql-component.googleapis.com",
  "storage-api.googleapis.com",
  "storage-component.googleapis.com",
  "sts.googleapis.com",
]
iam_policy = {
  "roles/owner" = {
    users = [
      "prasetiyohadi92@gmail.com",
    ]
  }
}
