resource "google_service_account" "app_terraform_io" {
  account_id   = "app-terraform-io"
  description  = "Service account for app.terraform.io"
  display_name = "app-terraform-io"
  project      = "deuterion-net"
}
# terraform import google_service_account.app_terraform_io projects/deuterion-net/serviceAccounts/app-terraform-io@deuterion-net.iam.gserviceaccount.com
resource "google_service_account" "cloud_dns" {
  account_id   = "cloud-dns"
  disabled     = true
  display_name = "cloud-dns"
  project      = "deuterion-net"
}
# terraform import google_service_account.cloud_dns projects/deuterion-net/serviceAccounts/cloud-dns@deuterion-net.iam.gserviceaccount.com
resource "google_service_account" "deuterion_net_dev" {
  account_id   = "deuterion-net-dev"
  disabled     = true
  description  = "Service account for app.terraform.io workspace deuterion-net-dev"
  display_name = "deuterion-net-dev"
  project      = "deuterion-net"
}
# terraform import google_service_account.deuterion_net_dev projects/deuterion-net/serviceAccounts/deuterion-net-dev@deuterion-net.iam.gserviceaccount.com

# Service Account for HashiCorp Vault
resource "google_service_account" "vault" {
  account_id   = "sa-vault"
  description  = "Service account for HashiCorp Vault"
  display_name = "sa-vault"
  project      = "deuterion-net"
}
