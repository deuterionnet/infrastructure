resource "google_project" "main" {
  name                = local.project_id
  project_id          = local.project_id
  billing_account     = var.billing_account
  auto_create_network = false
  labels              = merge(local.common_labels, { component = "project" })
}

resource "google_project_service" "main" {
  for_each = toset(var.project_services)

  project                    = google_project.main.number
  service                    = each.value
  disable_dependent_services = true
  disable_on_destroy         = false
}

resource "google_project_iam_policy" "main" {
  project     = google_project.main.project_id
  policy_data = data.google_iam_policy.main.policy_data

  depends_on = [
    google_project_service.main,
  ]
}

# resource "google_compute_project_metadata_item" "enable_oslogin" {
#   project = google_project.main.project_id
#
#   key   = "enable-oslogin"
#   value = upper(tostring(var.enable_oslogin))
# }

# resource "google_compute_shared_vpc_service_project" "main" {
#   provider = google-beta
#
#   for_each = toset(local.shared_vpc_host_project_ids)
#
#   host_project    = each.value
#   service_project = google_project.main.project_id
# }

# locals {
#   shared_vpc_host_project_ids = var.enable_shared_vpc ? [
#     module.remote_states.current.network.outputs.shared_vpc_host_project,
#   ] : []
# }
