# Resources identity variables

locals {
  common_labels = {
    product = var.product
    stack   = var.stack
    env     = var.env
    app     = var.app
    # Provider's specific labels
    region = var.region
  }

  # common_tags = local.common_labels
}

variable "product" {
  type        = string
  description = "The infrastructure product name, used in resource naming"
}

variable "stack" {
  type        = string
  description = "The infrastructure stack name, used in resource naming"
}

variable "env" {
  type        = string
  description = "The infrastructure environment, used in resource naming"
}

variable "app" {
  type        = string
  default     = ""
  description = "The application using the infrastructure"
}

# Resources provider variables

variable "region" {
  type        = string
  default     = "us-west1"
  description = "The Google Cloud region"
}

variable "project_id" {
  type        = string
  default     = ""
  description = "The Google Cloud project ID"
}

# Resources variables

variable "billing_account" {
  type        = string
  default     = ""
  description = "The Google Cloud billing account"
}

# variable "folder_id" {
#   type        = string
#   default     = ""
#   description = "The Google folder ID of the project"
# }

variable "project_services" {
  type        = list(string)
  default     = []
  description = "Google Cloud project's enabled APIs"
}

variable "iam_policy" {
  type = map(object({
    users            = optional(list(string), [])
    groups           = optional(list(string), [])
    service_accounts = optional(list(string), [])
  }))
  default     = {}
  description = "The environment specific IAM policy for Google Cloud project"
}

# variable "enable_oslogin" {
#   type        = bool
#   default     = true
#   description = "Whether to enable oslogin for this project"
# }

# variable "enable_shared_vpc" {
#   type        = bool
#   default     = true
#   description = "Whether to enable this project to share the vpc of the network project"
# }

# variable "instance_specs" {
#   type = map(object({
#     count             = string
#     type              = string
#     data_disk_size_gb = number
#   }))
#   default     = {}
#   description = "General variable that can control virtual machine count and type in different environments"
# }
