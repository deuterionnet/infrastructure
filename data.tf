data "google_iam_policy" "main" {
  dynamic "binding" {
    for_each = local.iam_policy

    content {
      role = binding.key

      members = concat(
        formatlist("user:%s", lookup(binding.value, "users", [])),
        formatlist("group:%s", lookup(binding.value, "groups", [])),
        formatlist("serviceAccount:%s", lookup(binding.value, "service_accounts", [])),
      )
    }
  }
}

locals {
  iam_policy = merge(local.base_iam_policy, var.iam_policy)

  base_iam_policy = {
    # Google cloud IAM policy for service APIs' service account
    #
    # Initial common policy
    "roles/editor" = {
      service_accounts = [
        local.default_service_account_emails.compute,
        local.default_service_account_emails.cloud_services,
        local.default_service_account_emails.container_registry,
      ]
    }

    # Initial common policy
    "roles/compute.serviceAgent" = {
      service_accounts = [
        local.default_service_account_emails.compute_system,
      ]
    }

    # IAM policy when enabling Cloud Asset Google API
    "roles/cloudasset.serviceAgent" = {
      service_accounts = [
        local.default_service_account_emails.cloud_asset,
      ]
    }

    # IAM policy when enabling Container Google API
    "roles/container.serviceAgent" = {
      service_accounts = [
        local.default_service_account_emails.container,
      ]
    }

    # IAM policy when enabling Artifact Registry Google API
    "roles/artifactregistry.serviceAgent" = {
      service_accounts = [
        local.default_service_account_emails.artifact_registry,
      ]
    }

    # # IAM policy when enabling Service Networking Google API
    # "roles/servicenetworking.serviceAgent" = {
    #   service_accounts = [
    #     local.default_service_account_emails.service_networking,
    #   ]
    # }

    # # IAM policy when enabling Google Cloud Memorystore for Redis API
    # "roles/redis.serviceAgent" = {
    #   service_accounts = [
    #     local.default_service_account_emails.redis,
    #   ]
    # }

    # Additional IAM policy
    #
    # IAM policy for DNS Administrator
    "roles/dns.admin" = {
      service_accounts = [
        google_service_account.cloud_dns.email,
      ]
    }

    # # IAM policy to allow Google Cloud Engine to authenticate to HashiCorp Vault
    # "roles/iam.serviceAccountKeyAdmin" = {
    #   service_accounts = [
    #     module.remote_states.current.vault.outputs.vault_service_account_email,
    #   ]
    # }

    # # IAM policy to allow Google Cloud Engine to authenticate to HashiCorp Vault
    # "roles/compute.viewer" = {
    #   service_accounts = [
    #     module.remote_states.current.vault.outputs.vault_service_account_email,
    #   ]
    # }
  }

  # Map of Google Cloud services' default service account email
  default_service_account_emails = {
    cloud_services     = "${google_project.main.number}@cloudservices.gserviceaccount.com"
    compute            = "${google_project.main.number}-compute@developer.gserviceaccount.com"
    compute_system     = "service-${google_project.main.number}@compute-system.iam.gserviceaccount.com"
    container          = "service-${google_project.main.number}@container-engine-robot.iam.gserviceaccount.com"
    container_registry = "service-${google_project.main.number}@containerregistry.iam.gserviceaccount.com"
    pubsub             = "service-${google_project.main.number}@gcp-sa-pubsub.iam.gserviceaccount.com"
    redis              = "service-${google_project.main.number}@cloud-redis.iam.gserviceaccount.com"
    service_networking = "service-${google_project.main.number}@service-networking.iam.gserviceaccount.com"
    cloud_asset        = "service-${google_project.main.number}@gcp-sa-cloudasset.iam.gserviceaccount.com"
    artifact_registry  = "service-${google_project.main.number}@gcp-sa-artifactregistry.iam.gserviceaccount.com"
  }
}

# The default Google Storage Bucket IAM policy
data "google_iam_policy" "default_bucket" {
  dynamic "binding" {
    for_each = local.default_bucket_iam_policy

    content {
      role = binding.key

      members = concat(
        formatlist("projectEditor:%s", lookup(binding.value, "project_editors", [])),
        formatlist("projectOwner:%s", lookup(binding.value, "project_owners", [])),
        formatlist("projectViewer:%s", lookup(binding.value, "project_viewers", []))
      )
    }
  }
}

# The IAM policy required to allow public access to objects in bucket
data "google_iam_policy" "public_object" {
  dynamic "binding" {
    for_each = local.default_bucket_iam_policy

    content {
      role = binding.key

      members = concat(
        formatlist("projectEditor:%s", lookup(binding.value, "project_editors", [])),
        formatlist("projectOwner:%s", lookup(binding.value, "project_owners", [])),
        formatlist("projectViewer:%s", lookup(binding.value, "project_viewers", []))
      )
    }
  }

  # Custom IAM policy
  binding {
    role = "roles/storage.legacyObjectReader"
    members = [
      # Allow public access to object
      "allUsers",
    ]
  }
}

# The IAM policy required to allow vault service account to objects in bucket
data "google_iam_policy" "vault" {
  dynamic "binding" {
    for_each = local.default_bucket_iam_policy

    content {
      role = binding.key

      members = concat(
        formatlist("projectEditor:%s", lookup(binding.value, "project_editors", [])),
        formatlist("projectOwner:%s", lookup(binding.value, "project_owners", [])),
        formatlist("projectViewer:%s", lookup(binding.value, "project_viewers", []))
      )
    }
  }

  # Custom IAM policy
  binding {
    role = "roles/storage.objectAdmin"
    members = [
      "serviceAccount:${google_service_account.vault.email}",
    ]
  }
}

locals {
  # Default bucket IAM policy
  default_bucket_iam_policy = {
    "roles/storage.legacyBucketOwner" = {
      project_editors = [var.project_id]
      project_owners  = [var.project_id]
    }

    "roles/storage.legacyBucketReader" = {
      project_viewers = [var.project_id]
    }

    "roles/storage.legacyObjectOwner" = {
      project_editors = [var.project_id]
      project_owners  = [var.project_id]
    }

    "roles/storage.legacyObjectReader" = {
      project_viewers = [var.project_id]
    }
  }
}
