# Custom role for Terraform
resource "google_project_iam_custom_role" "terraform_sa" {
  description = "Managed by Terraform"
  permissions = [
    "compute.disks.create",
    "compute.instances.create",
    "compute.instances.delete",
    "compute.instances.get",
    "compute.instances.setScheduling",
    "compute.instances.setTags",
    "compute.networks.create",
    "compute.networks.delete",
    "compute.networks.get",
    "compute.subnetworks.use",
    "compute.subnetworks.useExternalIp",
    "compute.zones.get"
  ]
  project = "deuterion-net"
  role_id = "TerraformSA"
  title   = "TerraformSA"
}
# terraform import google_project_iam_custom_role.terraform_sa projects/deuterion-net/roles/TerraformSA
