# Private bucket
resource "google_storage_bucket" "private" {
  name                        = "sb-${var.product}-${var.env}-${var.stack}-private"
  project                     = google_project.main.project_id
  location                    = upper(var.region)
  force_destroy               = false
  public_access_prevention    = "inherited"
  storage_class               = "STANDARD"
  uniform_bucket_level_access = true
  labels                      = merge(local.common_labels, { component = "sb" })

  depends_on = [
    google_project_service.main
  ]
}

# Apply default bucket IAM policy for private bucket, see data.tf for other IAM policy definitions
resource "google_storage_bucket_iam_policy" "private" {
  bucket      = google_storage_bucket.private.name
  policy_data = data.google_iam_policy.default_bucket.policy_data
}

# Private vault bucket
resource "google_storage_bucket" "vault" {
  name                        = "sb-${var.product}-${var.env}-${var.stack}-vault"
  project                     = google_project.main.project_id
  location                    = upper(var.region)
  force_destroy               = false
  public_access_prevention    = "inherited"
  storage_class               = "STANDARD"
  uniform_bucket_level_access = true
  labels                      = merge(local.common_labels, { component = "sb" })

  depends_on = [
    google_project_service.main
  ]
}

# Apply default bucket IAM policy for private bucket, see data.tf for other IAM policy definitions
resource "google_storage_bucket_iam_policy" "vault" {
  bucket      = google_storage_bucket.vault.name
  policy_data = data.google_iam_policy.vault.policy_data
}

# Public bucket
resource "google_storage_bucket" "public" {
  name                        = "sb-${var.product}-${var.env}-${var.stack}-public"
  project                     = google_project.main.project_id
  location                    = upper(var.region)
  force_destroy               = false
  public_access_prevention    = "inherited"
  storage_class               = "STANDARD"
  uniform_bucket_level_access = true
  labels                      = merge(local.common_labels, { component = "sb" })

  depends_on = [
    google_project_service.main
  ]
}

# Apply public object IAM policy for public bucket, see data.tf for other IAM policy definitions
resource "google_storage_bucket_iam_policy" "public_object" {
  bucket      = google_storage_bucket.public.name
  policy_data = data.google_iam_policy.public_object.policy_data
}

# Terraform backend bucket
resource "google_storage_bucket" "tf_backend" {
  name                        = "sb-${var.product}-${var.env}-${var.stack}-tf-backend"
  project                     = google_project.main.project_id
  location                    = upper(var.region)
  force_destroy               = false
  public_access_prevention    = "inherited"
  storage_class               = "REGIONAL"
  uniform_bucket_level_access = true
  labels                      = merge(local.common_labels, { app = "terraform", component = "sb" })

  versioning {
    enabled = true
  }

  depends_on = [
    google_project_service.main
  ]
}

# Apply default bucket IAM policy for private bucket, see data.tf for other IAM policy definitions
resource "google_storage_bucket_iam_policy" "tf_backend" {
  bucket      = google_storage_bucket.tf_backend.name
  policy_data = data.google_iam_policy.default_bucket.policy_data
}
