resource "google_artifact_registry_repository" "main" {
  location      = var.region
  repository_id = "registry"
  description   = "OCI registry"
  format        = "DOCKER"
}
