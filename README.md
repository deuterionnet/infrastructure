# Infrastructure

Infrastructure as Code for deuterion-net environment.

## Troubleshooting

### Importing logging sink from output of Google Cloud exporter

Using steps described in [Export your Google Cloud resources into Terraform format](https://cloud.google.com/docs/terraform/resource-management/export#before_you_begin),
we can get the state of our current Google Cloud project in HashiCorp Language.
But currently there are some errors in the exported Terraform files,
such as [unknown resource type: `google_logging_log_sink`](https://stackoverflow.com/questions/72445305/gcloud-terraform-export-unknown-resource-type-google-logging-log-sink).
These are solution to fix this issue,

1. Change `google_logging_log_sink` to `google_logging_project_sink` in the
   Terraform file
2. Import the resource using the new resource type and modify the address,

  ```bash
  terraform import google_logging_project_sink.<RESOURCE_NAME> projects/<PROJECT_NUMBER>/sinks/<NAME>
  ```

Similar issue also happens with unknown resource type:
`google_iam_custom_role`. We can use similar solution,

1. Change `google_iam_custom_role` to `google_project_iam_custom_role` in the
   Terraform file
2. Import the resource using the new resource type and modify the address,

  ```bash
  terraform import google_project_iam_custom_role.<RESOURCE_NAME> projects/<PROJECT_NAME>/roles/<NAME>
  ```
