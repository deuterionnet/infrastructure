# Global local variables, local variables that are used in multiple files
locals {
  project_id = var.project_id == "" ? "${var.product}-${var.env}-${var.stack}" : var.project_id
}
