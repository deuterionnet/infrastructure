# Public DNS managed zone
resource "google_dns_managed_zone" "deuterion_net" {
  dns_name = "deuterion.net."

  dnssec_config {
    default_key_specs {
      algorithm  = "rsasha256"
      key_length = 2048
      key_type   = "keySigning"
      kind       = "dns#dnsKeySpec"
    }

    default_key_specs {
      algorithm  = "rsasha256"
      key_length = 1024
      key_type   = "zoneSigning"
      kind       = "dns#dnsKeySpec"
    }

    kind          = "dns#managedZoneDnsSecConfig"
    non_existence = "nsec3"
    state         = "on"
  }

  force_destroy = false
  name          = "deuterion"
  project       = "deuterion-net"
  visibility    = "public"
}
