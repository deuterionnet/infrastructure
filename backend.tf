terraform {
  backend "gcs" {
    bucket = "sb-dn-dev-infra-tf-backend"
    prefix = "dn/dev/infra"
  }
}
